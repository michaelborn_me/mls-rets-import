<cfscript>
/**
    * 
    * GET OBJECT METADATA AND DUMP IT OUT
    * Just debugging.
*/
writeOutput('<h3 class="title is-3">Get Object Metadata</h3>');

// iterate through search IDs, search for each.
local.resources = ["Office","Property"];
for (local.resource in local.resources) {
    writeOutput('<h4 class="title is-4">Object metadata for ' & local.resource & '</h4>');

    local.info = retsSession.getMetadata(metaRequest.init("OBJECT",local.resource));
    if (ArrayLen(local.info.getMetadata())) {
        metadata.dumpMetadata(local.info.getMetadata());
    } else {
        writeOutput('<p class="notification is-warning">No metadata found for this resource.</p>');
    }
}
</cfscript>