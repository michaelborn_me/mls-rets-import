<cfscript>
/**
    * 
    * GET CLASS METADATA AND DUMP IT OUT
    * Just debugging.
    * Note that Agent, Media, OpenHouse all have a single class, but Property has 6 classes:
    * Commercial,Keeplist,LotsAndLand,MobileHome,MultiFamily,ResidentialProperty
*/
writeOutput('<h3 class="title is-3">Get Classes</h3>');

// iterate through search IDs, search for each.
local.resources = ["Agent","Media","Office","OpenHouse","Property"];
for (local.resource in local.resources) {
    writeOutput('<h4 class="title is-4">Classes for ' & local.resource & '</h4>');

    local.info = retsSession.getMetadata(metaRequest.init("CLASS",local.resource));
    if (ArrayLen(local.info.getMetadata())) {
        metadata.dumpMetadata(local.info.getMetadata());
    } else {
        writeOutput('<p class="notification is-warning">No metadata found for this resource.</p>');
    }
}
</cfscript>