<cfscript>
/**
    * 
    * GET TABLE METADATA AND DUMP IT OUT
    * Just debugging.
*/
writeOutput('<h3 class="title is-3">Get Table Metadata</h3>');

// iterate through search IDs, search for each.
local.resources = ["Agent","Media","Office","OpenHouse","Property"];
for (local.resource in local.resources) {
    writeOutput('<h4 class="title is-4">Table metadata for ' & local.resource & '</h4>');

    local.info = retsSession.getMetadata(metaRequest.init("TABLE",local.resource));
    if (ArrayLen(local.info.getMetadata())) {
        metadata.dumpMetadata(local.info.getMetadata());
    } else {
        writeOutput('<p class="notification is-warning">No metadata found for this resource.</p>');
    }
}
</cfscript>