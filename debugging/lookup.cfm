<cfscript>
/**
    * 
    * 
    * 
    * GET LOOKUPS AND DUMP THEM OUT
    * 
    * To do this properly, we loop through the records from getMetadata()
    * and for each record, loop over getKnownAttributes().
    * Then, for each known attribute, we save it to the database
    * in the appropriate (meta_table,meta_resource,meta_lookup, etc.) table.
    *
    * 
    * 
*/
local.info = retsSession.getMetadata(metaRequest.init("LOOKUP_TYPE","Property:*"));
writeOutput('<h3 class="title is-3">get lookups(Property:*)</h3>');

metadata.dumpMetadata(local.info.getMetadata());

</cfscript>