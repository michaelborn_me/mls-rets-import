<cfscript>
/**
    * 
    * GET RESOURCE METADATA AND DUMP IT OUT
    * Just debugging.
*/
writeOutput('<h3 class="title is-3">Get Resources</h3>');
local.info = retsSession.getMetadata(metaRequest.init("RESOURCE","*"));
metadata.dumpMetadata(local.info.getMetadata());
</cfscript>