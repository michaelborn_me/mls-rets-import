<div class="hero is-dark">
	<div class="hero-body">
		<div class="container">
			<h2 class="title">Import Metadata</h2>
            <h3 class="subtitle">RETS request: GetMetadata</h3>
		</div>
	</div>
</div>
<cfscript>
	// Start RETS connection
	include "inc/start.cfm";
	
	// schedule the task
	cronURL = "http://#cgi.server_name##cgi.script_name#";
	cfschedule(
		action		= "update",
		task		= "cny.mls.import.meta",
		url			= "#cronURL#",
		interval	= config.interval.metadata,
		startDate	= "5/12/2016",
		startTime 	= "2:10 AM"
	);

	/**************
	 * BEGIN IMPORT
	 **************/
	writeOutput('<div class="section"><div class="container"><h2 class="title is-2">Pull Metadata</h2>');
	metadata = CreateObject("beans.metadata").init(
		datasource = variables.config.datasource
	);
    
    // get ready for request
    metaRequest = javaloader.create("org.realtors.rets.client.GetMetadataRequest");
    
	// for each table type, loop through and get all lookup types.
	local.tables = ["Agent","Media","Property","OpenHouse"];
	for (local.table in local.tables) {
		/**
		* STEP ONE
		* Get CLASS metadata for each TABLE.
		*/
		local.info = retsSession.getMetadata(metaRequest.init("CLASS",local.table));
		local.classes = local.info.getMetadata();
		if ( variables.config.debug ) {
			metadata.dumpMetadata(local.classes);
		}

		// now loop through each class.
		// class might be Agent:Agent, or it might be Property:Commercial.
		for (local.class in local.classes) {
			local.classname = local.class.getClassName();
			
			/**
			* STEP TWO
			* Get COLUMN information for each CLASS. (Remember, class is a subset or type of TABLE.)
			* So, for example, the Property:ResidentialProperty table metadata will return 20-30 rows.
			* Each row tells us about a column in the ResidentialProperty table
			* which is a lookup field.
			* For example, Heat or OfficeStatus.
			* Heat is stored as an integer.
			* officeStatus as A-Z.
			*/
			writeOutput('<h3 class="title is-3">Pulling metadata for the #local.classname# class</h3>');
			local.info = retsSession.getMetadata(metaRequest.init("TABLE",local.table & ":" & local.classname));
			local.fields = local.info.getMetadata();

			if ( !ArrayLen(local.fields) ) {
				writeOutput('<p class="notification is-warning">No fields found for this table</p>');
			}

			if ( variables.config.debug ) {
				metadata.dumpMetadata(local.fields);
			}

			
			/**
			* STEP THREE
			* Get LOOKUP metadata to tell us the values for a lookup field.
			* getMetadata?Type=METADATA-LOOKUP_TYPE&ID=Property:Lookup41
			* 
			* So, for example, the Office class has an OfficeStatus lookup field.
			* Stored as A-Z, but the real values are in the lookup field.
			* 
			* the STEP ONE for the Commercial class might tell us there's a lookup
			* for OfficeStatus and the lookup is called Lookup41.
			* Then the API call is:
			* getMetadata?Type=METADATA-LOOKUP_TYPE&ID=Property:Lookup41
			*/
			
			for (local.field in local.fields) {
				
				local.lookupName = trim(local.field.getAttribute("LookupName"));
				if ( local.field.isLookup() ) {
					// get lookups by class and by lookup id
					local.lookupPath = local.table & ":" & local.lookupName;

					writeOutput('<h4 class="title is-4">Pull #local.field.getSystemName()# field lookup - path: #local.lookupPath#</h4>');
					local.info = retsSession.getMetadata(metaRequest.init("LOOKUP_TYPE",local.lookupPath));

					if ( !isArray(local.info.getMetadata()) ) {
						writeOutput('<p class="notification is-danger">Found nothing - bad lookup path?</p>');
						abort;
					}
					// save to DB
					if (ArrayLen(local.info.getMetadata())) {
						local.savedRows = metadata.saveLookups(local.info.getMetadata(),local.field.getAttribute("DBName"), local.classname);
						writeOutput('<p class="notification is-success">Saved ' & local.savedRows & ' lookup field values</p>');
					} else {
						writeOutput('<p class="notification is-warning">No lookup field values for this field</p>');
					}
					if ( variables.config.debug ) {
						metadata.dumpMetadata(local.info.getMetadata());
					}
				}// end (if proper lookup)
			}// loop to next field in the current class
		}// loop to next class for the current table, like "Property:Commercial" or "Property:Residential"
	}// loop to next table, like "Property" or "Media".

    // don't need this at the moment.
    // include "importmetadata/object.cfm";
    
    // don't need this at the moment.
    // include "importmetadata/class.cfm";

    // don't need this at the moment.
    // include "importmetadata/resource.cfm";
</cfscript>