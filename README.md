# CentralNewYork.com RETS Feed Plugin

Connect to a RETS server and pull MLS listings.

## Structure

* `loader.cfm` loads the Trulia-built java RETS client
* `auth.cfm` does the authentication with the RETS server
* `importX.cfm` is the scheduled job file for import X

## Imports (Scheduled Tasks)

* `importAgent.cfm` - import listing agents
* `importMedia.cfm` - metadata for all media objects. Not just images - also documents like `.xml` files.
* `importProperty.cfm` - import property listings.
* `importObjects.cfm` - import files, like binary images or whatever.

## Setup

1. Clone the repo.
2. Rename `config-sample.cfm` file as `config.cfm`
3. Update the RETS username and password.
4. Build the data schema by running `buildSQL.cfm`.
5. Run each of the schema builder queries on the `mysql` database server.
6. Run each import once to schedule the tasks.

## Database Setup

After running the `buildSQL.cfm` step, the tables are created according to the RETS metadata. However, some of the data is too long (thus contradicting the maxlength specified in the metadata) and thus require a longer column maxlength or switching to a `TEXT` datatype.

```sql
ALTER TABLE `ResidentialProperty`
MODIFY `DIRECTIONS` VARCHAR(255),
MODIFY `STREETNAMEIDX` VARCHAR(50),
MODIFY `OPENHOUSEREMARKS` TEXT,
MODIFY `POSTALCODE` VARCHAR(10),
MODIFY `TAXID` VARCHAR(55),
MODIFY `ZONING` VARCHAR(50);
```

```sql
ALTER TABLE `Commercial`
MODIFY `STREETNAMEIDX` VARCHAR(50),
MODIFY `DIRECTIONS` VARCHAR(255),
MODIFY `POSTALCODE` VARCHAR(10),
MODIFY `TAXID` VARCHAR(55),
MODIFY `ZONING` VARCHAR(50);
```

```sql
ALTER TABLE `LotsAndLand`
MODIFY `DIRECTIONS` VARCHAR(255),
MODIFY `POSTALCODE` VARCHAR(10),
MODIFY `STREETNAMEIDX` VARCHAR(50),
MODIFY `TAXID` VARCHAR(55),
MODIFY `ZONING` VARCHAR(50);
```

```sql
ALTER TABLE `MobileHome`
MODIFY `POSTALCODE` VARCHAR(10),
MODIFY `FUEL` VARCHAR(10),
MODIFY `OTHERNAME` VARCHAR(10),
MODIFY `STREETNAMEIDX` VARCHAR(50),
MODIFY `ZONING` VARCHAR(50);
```

```sql
ALTER TABLE `Agent`
MODIFY `AGENTTYPE` VARCHAR(15),
MODIFY `FAX` VARCHAR(15),
MODIFY `OFFICEPHONE` VARCHAR(15);
```

```sql
ALTER TABLE `MultiFamily`
MODIFY `DIRECTIONS` VARCHAR(255),
MODIFY `POSTALCODE` VARCHAR(10),
MODIFY `STREETNAMEIDX` VARCHAR(50),
MODIFY `TAXID` VARCHAR(55),
MODIFY `ZONING` VARCHAR(50);
```

```sql
ALTER TABLE openHouse
MODIFY ENDTIME VARCHAR(10),
MODIFY STARTTIME VARCHAR(10);
```

## Metadata table

This is a custom table layout (not from RETS) which is used by `importMetadata.cfm` to store the values for each lookup field. This is important in order to know what a "20" means in the Heat field for a ResidentialProperty listing, for example.

```sql
CREATE TABLE `Metadata` (
  `LongValue` varchar(100) DEFAULT NULL,
  `MetadataEntryID` varchar(100) DEFAULT NULL,
  `ShortValue` varchar(50) DEFAULT NULL,
  `Value` varchar(100) DEFAULT NULL,
  `ID` varchar(50) DEFAULT NULL,
  `Path` varchar(50) NOT NULL,
  `DBField` varchar(50) NOT NULL,
  PRIMARY KEY (`Path`),
  UNIQUE KEY `Path` (`Path`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
```

This sql statement is not run by `buildSQL.cfm`, it's an additional step and should be run manually.

## Running the Imports

Run each import once to schedule the tasks.

* importProperty.cfm
* importMedia.cfm
* importAgent.cfm
* importMetadata.cfm
* importObjects.cfm