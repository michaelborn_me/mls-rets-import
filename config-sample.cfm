<cfscript>
config = {
	rets_uri = "http://example.com/gru/login",
	creds = {
		username = "getthisfrommlsboard",
		password = "sorrynotsorry",
		expire_date = "NOT_USED_BUT_STILL_USEFUL_TO_KNOW"
	},
	
	// number of seconds between times this runs.
	// 14400 = 60 seconds per minute times 60 minutes per hour times 4 hours.
	interval = {
		agent = "daily",
		property = "14400",
		media = "14400",
		object = "14400",
		metadata = "weekly",
		openhouse = "daily"
	},

	// group any sql-specific settings here
	sql = {
		// this is only utilized during setup (see buildSQL.cfm)
		// it specifies whether or not to automatically create the database tables you need
		// for each data type, like ResidentialProperty, Media, or LotsAndLand.
		// if this is false, you will have to copy/paste/run the outputted SQL statement using a sql program like Sequel Pro. (or command line.)
		// turn this off if CREATE and ALTER permissions are DISABLED for your mysql datasource.
		createtables = true
	},

	// while grabbing listings/records, how many records to get per request?
	// the larger the data set, the lower this number should be so that MYSQL can still process the large queries.
	batchSize = {
		residential = 100,
		agents = 500,
		media = 1000,
		binaries = 100,
		openhouse = 500
	},

	// use search.type to limit the records returned for the agent,media and property data types.
	// 
	// ModificationTimestamp = '2017-11-01',
	// Why? while it's cool to try to get all listing photos for every listing ever sold,
	// we simply can't justify retrieving and storing 100,000+ photos for old junk.
	//
	// OPTION ONE
	// put in a hardcoded date to retrieve listings since date x,
	// ModificationTimestamp = '2017-11-01',
	// 
	// OPTION TWO
	// use a calculated date like today minus 1 year.
	// ModificationTimestamp = DateFormat(DateAdd('yyyy',-1, now()),'yyyy-mm-dd')
	// Note that once the import is running, it can be sped up immensely 
	// by only retrieving the items modified in the last day or two.
	// ModificationTimestamp = DateFormat(DateAdd("d",-1, now()),'yyyy-mm-dd')
	//
	// Filter out "For Lease" property types.
	// Don't pull these! See syntax: ~D means NOT d values.
	// (PropertyType=~D)
	
	// ONLY PULL PHOTO MEDIA OBJECTS
	// (MediaType=photo)
	search = {
		agent = "(ModificationTimestamp=#DateFormat(DateAdd('yyyy',-10, now()),'yyyy-mm-dd')#+),(FirmID=COLF01)",
		media = "(ModificationTimestamp=#DateFormat(DateAdd('m',-1, now()),'yyyy-mm-dd')#+),(MediaType=photo)",
		property = "(ModificationTimestamp=#DateFormat(DateAdd('m',-1, now()),'yyyy-mm-dd')#+),(PropertyType=~D),(ListingStatus=|A,K,P)",
		openhouse = "(ModificationTimestamp=#DateFormat(DateAdd('m',-1, now()),'yyyy-mm-dd')#+),(HOSTOFFICEUID=6395COLF010)",
		// please note that this is ONLY a date string, and it is meant for SQL, NOT for the RETS queries.
		binaries = DateFormat(DateAdd('m',-1, now()),'yyyy-mm-dd')
	},
	
	// specify where to save media objects relating to a listing or agent or whatever
	folders = {
		images = "/app/uploads/listings/images/",
		videos = "/app/uploads/listings/videos/",
		documents = "/app/uploads/listings/docs/",
	},
	
	// turn on debugging to show helpful logs in cron jobs.
	debug: true,

	// what database to save data to / pull from?
	datasource: "blabbitybla"
};
</cfscript>