component {
    // this file mostly created to avoid running the CMS tspark application.cfc.

    // make sure any code can reference the lib directory
    this.mappings["/lib"] = getDirectoryFromPath(getCurrentTemplatePath()) & "lib";
}