<div class="hero is-dark">
	<div class="hero-body">
		<div class="container">
			<h2 class="title">Build RETS Database Schema</h2>
            <h3 class="subtitle">For each class, create a table. For each table metadata, create a column.</h3>
		</div>
	</div>
</div>
<cfscript>
/**
    * 
    * this file loops through all table metadata
    * and builds a SQL statement for each table.
*/
include "inc/start.cfm";

/**************
* BEGIN SCHEMA BUILDING
**************/
writeOutput('<div class="section"><div class="container"><h2 class="title is-2">Create Tables</h2>');
metadata = CreateObject("beans.metadata").init(
    datasource = variables.config.datasource
);

// get ready for request
metaRequest = javaloader.create("org.realtors.rets.client.GetMetadataRequest");

// get all resources
local.info = retsSession.getMetadata(metaRequest.init("RESOURCE","*"));
local.resources = local.info.getMetadata();
if (config.debug) {
    metadata.dumpMetadata(local.resources);
}

// Get normal table data
if ( ArrayLen(local.resources) ) {
    for (local.resource in local.resources) {
        // iterate through resources, get classes for each
        local.classes = retsSession.getMetadata(metaRequest.init("CLASS",local.resource.getStandardName()));
        
        if (ArrayLen(local.classes.getMetadata())) {
            if ( variables.config.debug ) {
                // it's helpful to see what "classes" we are dealing with.
                metadata.dumpMetadata( local.classes.getMetadata() );
            }
            
            for (local.class in local.classes.getMetadata()) {
                // create table for each CLASS type within each resource.
                local.columns = retsSession.getMetadata(metaRequest.init("TABLE",local.class.getPath()));
                if (ArrayLen(local.columns.getMetadata())) {
                    writeOutput('<div class="section"><h3 class="title is-3">Create Table Schema for ' & local.class.getPath() & '</h3>');
                    
                    if ( variables.config.debug ) {
                        metadata.dumpMetadata( local.columns.getMetadata() );
                    }
                    
                    // build a statement to create a table for the current resource.
                    local.sql = "CREATE TABLE IF NOT EXISTS `" & local.class.getClassName() & "` (";
                    
                    // create columns
                    local.sql &= metadata.generateSchema(
                        tablename = local.resource,
                        columns = local.columns.getMetadata()
                    );
                    
                    // make sure to save the key field.
                    local.sql &= 'PRIMARY KEY(' & local.resource.getKeyField() & ')' & Chr(10) & ')';
                    
                    writeOutput('<pre>' & local.sql & '</pre>');

                    // create/update the table.
                    if ( variables.config.sql.createtables ) {
                        local.qry = queryExecute(local.sql,{},{datasource: config.datasource});
                        writeDump(local.qry);
                    }
                    writeOutput("</div>");
                }
            }// end class loop
        } else {
            writeOutput('<p class="notification is-warning">No metadata found for this resource.</p>');
        }

    }// end resource loop
}
</cfscript>