<div class="hero is-dark">
	<div class="hero-body">
		<div class="container">
			<h2 class="title">Remove Duplicate Listings</h2>
		</div>
	</div>
</div>
<cfscript>
	// Start RETS connection
	include "inc/start.cfm";
	
	// schedule the task
	cronURL = "http://#cgi.server_name##cgi.script_name#";
	cfschedule(
		action		= "update",
		task		= "cny.mls.import.removeExpired",
		url			= "#cronURL#",
		interval	= config.interval.property,
		startDate	= "5/12/2016",
		startTime 	= "2:00 AM"
	);

	/**************
	 * BEGIN IMPORT
	 **************/
	local.deletedCount = 0;
	listingObj = CreateObject("beans.property").init(
		datasource = variables.config.datasource
	);

	local.classes = ["Commercial","LotsAndLand","MobileHome","MultiFamily","ResidentialProperty"];
	for (local.class in local.classes) {
		local.currentBatch = 0;
		writeOutput('
			<div class="section">
				<div class="container">
					<h2 class="title is-2">Remove #local.class# Listings</h2>
					<h3 class="subtitle">Search query: #variables.config.search.property#,(ListingID=X)</h3>');
		
		do {
			// grab X records by Y batch from table Z.
			variables.allListings = listingObj.GetAll(
				limit = config.batchSize.residential,
				offset = local.currentBatch * config.batchSize.residential,
				tablename = local.class
			);

			writeOutput('<h3 class="title is-5">Checking #local.class# properties</h3><p>#variables.allListings.recordCount# result(s) in batch ###local.currentBatch#</p>');

			cfloop( query = variables.allListings ) {
				// search for this listing by ID, in addition to making sure
				// it matches the other config-specified search criteria.
				variables.searchQuery = "#variables.config.search.property#,(ListingID=#variables.allListings.listingID#)"
				local.searchRequest = javaloader.create("org.realtors.rets.client.SearchRequest").init("Property",local.class,variables.searchQuery);

				// We should get a single record back. If we get zero results, then this listing is old / expired.
				local.searchRequest.setLimit(1);
				local.searchResponse = retsSession.search(local.searchRequest);
				local.rowCount = local.searchResponse.getRowCount();

				if (local.searchResponse.getRowCount() > 0) {
					// be silent on success, loud on danger.
					// writeOutput('<p class="notification is-success">This record is up to date.</p>');
				} else {
					writeOutput('<p class="notification is-warning">This record is not in the RETS: #variables.allListings.listingID#.</p>');
					// run the query, given all the data we have
					local.sqlSuccess = listingObj.deleteByListingID(
						ListingID = variables.allListings.listingID,
						tablename = local.class
					);
					
					if ( local.sqlSuccess.recordCount == 1) {
						// affected 1 record.
						local.deletedCount++;
						writeOutput('<p class="notification is-success">DELETED listing record.</p>');
					}
				}
			}
			
			// increment the current batch number so we can get the NEXT batch
			local.currentBatch++;
			// as long as we were able to retrieve the max, then keep looping!
		} while(local.searchResponse.getRowCount() == config.batchSize.residential);
		
		writeOutput('</div></div>');
	}

	// success
	writeoutput('<p class="notification is-success">Done! Removed ' & local.deletedCount & " old listings from the database.</p>");
	writeOutput('</div></div>');
</cfscript>