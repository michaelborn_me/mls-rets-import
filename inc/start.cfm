<cfscript>
/**************
* CONFIG SETTINGS
**************/
include "../config.cfm";

/**************
* START JAVALOADER
**************/
// We use javaloader to load a jar and thus allow us to reuse the java classes.
LOCAL.loadPaths = [ExpandPath("lib/retsclient/target/rets-client-local-jar-with-dependencies.jar")];
javaloader = createObject("component", "lib.javaloader.javaloader.JavaLoader").init( LOCAL.loadPaths );

// Nice themeing for imports
writeOutput('<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bulma/0.6.2/css/bulma.min.css" />');

/**************
* CONNECT
**************/
include "auth.cfm";
</cfscript>