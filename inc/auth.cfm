<div class="section">
	<div class="container">
		<h2 class="title is-2">Connecting to RETS server</h2>
<cfscript>
	// START RETS JAVA CLIENT
	httpClient = javaloader.create("org.realtors.rets.client.CommonsHttpClient").init();
	retsSession = javaloader.create("org.realtors.rets.client.RetsSession");
	retsVersion = javaloader.create("org.realtors.rets.client.RetsVersion");

	// set rets version
	retsVersion = retsVersion.init(1,7,2,0);
	
	// prepare for login!
	retsSession.init(config.rets_uri & "/login",httpClient,retsVersion);
	retsSession.setMethod("GET");
	
	// debugging
	writeOutput('<p class="notification is-success">Connected</p>');
	writeOutput('<p class="notification is-info">RETS URL: #retsSession.getLoginUrl()#<br>RETS Version: #retsVersion.toString()#</p>');
	
	// try to authenticate with the RETS server
	retsSession.login(config.creds.username, config.creds.password);
</cfscript>
	</div>
</div>