<div class="hero is-dark">
	<div class="hero-body">
		<div class="container">
			<h1 class="title">Import Listing Media</h1>
		</div>
	</div>
</div>
<cfscript>
	// Start RETS connection
	include "inc/start.cfm";
	
	// schedule the task
	cronURL = "http://#cgi.server_name##cgi.script_name#";
	cfschedule(
		action		= "update",
		task		= "cny.mls.import.media",
		url			= "#cronURL#",
		interval	= config.interval.media,
		startDate	= "5/12/2016",
		startTime 	= "2:20 AM"
	);

	/**************
	 * BEGIN IMPORT
	 **************/
	writeOutput('
		<div class="section">
			<div class="container">
				<h2 class="title is-2">Pull Listing Media</h2>
				<h3 class="subtitle">Search query: #variables.config.search.media#</h3>');
	
	mediaObj = CreateObject("beans.media").init(
		datasource = variables.config.datasource
	);
    
    currentBatch = 0;
	do {
		if (currentBatch > 0) {
			structClear(local.searchResponse);
		}
		// we grab the records in groups of 100
		// and store them in the db.
		
		// NOW DO A SEARCH
		searchFor = "Media";
		searchClass = "Media";
		// get only the records updated within the last year or month or whatever.
		// this makes queries for large sets of data just way faster.
		searchQuery = variables.config.search.media;
		searchRequest = javaloader.create("org.realtors.rets.client.SearchRequest").init(searchFor,searchClass,searchQuery);

		// get X records starting at Y where Y = (x*batchsize)
		searchRequest.setLimit(config.batchSize.media);
		searchRequest.setOffset(currentBatch * config.batchSize.media);
		local.searchResponse = retsSession.search(searchRequest);
		
		writeOutput('<h3 class="title is-5">Search Results: batch ##' & currentBatch & " found " & local.searchResponse.getRowCount() & " result(s)</h3>");
		
		if ( local.searchResponse.getRowCount() == 0 ) {
			// skip empty batch
			break;
		}

		// run the query, given all the data we have
		sqlSuccess = mediaObj.save(
			response = local.searchResponse
		);
		if (! sqlSuccess ) {
			// get error info
			writeOutput('<div class="notification is-danger"><p>ERROR: ' & mediaObj.getLatestError().detail & '</p>');
			writeDump(mediaObj.getLatestError(),false);
			writeOutput('</div>');
		} else {
			writeOutput('<p class="notification is-success">Saved results to database</p>');
		}
    
		// increment the current batch number so we can get the NEXT batch
		currentBatch++;

		// as long as we were able to retrieve the max, then keep looping!
	} while(local.searchResponse.getRowCount() == config.batchSize.media);

	// debugging
	writeoutput('<p class="notification is-success">Done! Saved ' & currentBatch & " batches of " & config.batchSize.media & ' each.</p>');
	writeOutput('</div></div>');
</cfscript>