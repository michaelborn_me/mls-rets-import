/**
 * Runs the query to save metadata to the database
 */
component hint="Runs the query to save metadata to the database" extends="bigQuery" {
	this.table = "Metadata";

    public numeric function saveLookups(required object metadata, required string dbField, required string tableName) {
        local.sqldata = [];

        // start the sql, enumerate columns
        local.sql = "INSERT INTO #this.table# (
        `LongValue`,`MetadataEntryID`,`ShortValue`,`Value`,`DBField`,`ID`,`Path`,`DBTable`
        )
        VALUES ";

        // now put in the fields
        local.count = 0;
        for (local.row in arguments.metadata) {
            // convert attribute Set to a string list which Coldfusion can use more easily.
            local.attributes = local.row.getKnownAttributes().toString();
            local.attributeArray = listToArray(REReplace(local.attributes,"[\[\]]","","ALL"));
            
            // separate multiple INSERT rows with a comma
            local.count++;
            if ( local.count > 1 ) { local.sql &=","; }
            
            // add each row to sql statement, making sure to use array-based positional syntax
            local.sql &= "(?,?,?,?,?,?,?,?)";
            
            // get dynamic columns, which are probably "LongValue,MetadataEntryID,Shortvalue,Value"
            for (local.column in local.attributeArray) {
                // add to data
                local.sqldata.append( local.row.getAttributeAsString( trim(local.column) ) );
            }

            // standard column for DBField so the custom code can easily pull values for a specific db field
            local.sqldata.append( arguments.dbField );
            
            // also get standard columns ID, Path and Table
            // so we can scope this data entry to the proper field/table combo.
            // see, without the "table" field, we don't know whether Heat=3 means Forced Air for LookupMulti1J
            // or whether it means Electric for LookupMulti3J.
            local.sqldata.append( local.row.getID() );
            local.sqldata.append( arguments.tableName & ":" & local.row.getPath() );
            local.sqldata.append( arguments.tableName );
        }
        local.sql &= "
        ON DUPLICATE KEY UPDATE
            `LongValue` = VALUES(`LongValue`),
            `MetadataEntryID` = VALUES(`MetadataEntryID`),
            `ShortValue` = VALUES(`ShortValue`),
            `Value` = VALUES(`Value`),
            `DBField` = VALUES(`DBField`),
            `DBTable` = VALUES(`DBTable`),
            `ID` = VALUES(`ID`)";
        // run query, duh
        local.qry = queryExecute(local.sql, local.sqldata, { datasource: this.datasource });
        return arrayLen(arguments.metadata);
    }

    public boolean function dumpMetadata(required object metadata, boolean showAll = true) {
        // make a nice scrollable box!
        writeOutput('<div style="max-width:100%;overflow:auto;max-height:80vh;"><table class="table">');

        // first print column headers
        this.dumpAttributeHeaders(arguments.metadata, arguments.showAll);

        // then dump the data
        this.dumpAttributes(arguments.metadata, arguments.showAll);

        writeOutput('</table></div>');

        return true;
    }

    public boolean function dumpAttributes(required object metadata, boolean showAll = true) {
        for (local.row in arguments.metadata) {
            // convert attribute Set to a string list which Coldfusion can use more easily.
            local.attributes = local.row.getKnownAttributes().toString();
            local.attributeArray = listToArray(REReplace(local.attributes,"[\[\]]","","ALL"));
            
            // print the column info
            writeOutput('<tr>');
            if (arguments.showAll) {
                // most columns are optional, but ID and Path are mandatory.
                for (local.column in local.attributeArray) {
                    writeOutput('<td data-type="' & local.column & '">');
                    writeOutput(local.row.getAttributeAsString(trim(local.column)));
                    
                    writeOutput('</td>');
                }
            }
            // these two attributes are standard across the board.
            writeOutput('<td>' & local.row.getId() & '</td>');
            writeOutput('<td>' & local.row.getPath() & '</td>');
            
            writeOutput('</tr>');
        }
        
        return true;
    }

    public boolean function dumpAttributeHeaders(required object metadata, boolean showAll = true) {
        // get first row just to check the columns.
        local.row = arguments.metadata[1];
        
        // get columns
        local.attributes = local.row.getKnownAttributes().toString();
        local.attributeArray = listToArray(REReplace(local.attributes,"[\[\]]","","ALL"));

        // print the column headers
        writeOutput('<thead><tr>');
        if (arguments.showAll) {
            // most columns are optional, but ID and Path are mandatory.
            for (local.column in local.attributeArray) {
                writeOutput('<th><strong>');
                writeOutput(local.column);
                writeOutput('</strong></th>');
            }
        }
        // these two attributes are standard across the board.
        writeOutput('<th><strong>ID</strong></th>');
        writeOutput('<th><strong>Path</strong></th>');

        writeOutput('</tr></thead>');

        return true;
    }



    public string function generateSchema(required string tablename, required object columns) {
        local.sql = "" & Chr(10);
        local.numberColumns = ArrayLen(arguments.columns);
        
        for (local.row in arguments.columns) {
            local.sql &= local.row.getDBName() & " ";
            local.sql &= this.getMysqlDataType( local.row );
            
            // separate multiple attributes with a comma
            local.sql &= "," & Chr(10);
        }
        return local.sql;
    }

    public string function getMysqlDataType(required object column) {
        local.correctDataType = "";
        switch(arguments.column.getAttributeAsString("DataType")) {
            case "Character":
                local.correctDataType = "VARCHAR("
                    & arguments.column.getAttributeAsString("maximumLength")
                    & ")";
            break;
            case "Int":
                local.correctDataType = "INTEGER("
                    & arguments.column.getAttributeAsString("maximumLength")
                    & ")";
            break;
            case "DateTime":
                local.correctDataType = "DateTime";
            break;
            case "Date":
                local.correctDataType = "Date";
            break;
            case "Decimal":
                local.correctDataType = "DECIMAL(" & arguments.column.getAttributeAsString("maximumLength")
                    & ","
                    & arguments.column.getAttributeAsString("precision")
                    & ")";
            break;
        }
        return local.correctDataType;
    }
}