/**
 * contains interactions with the database listingAgent table
 */
component hint="contains interactions with the database listingAgent table" extends="bigQuery" {
	this.table = "Agent";

}