/**
 * Runs the query to save properties to the database
 */
component hint="Runs the query to save properties to the database" extends="bigQuery" {
	this.table = "Property";
}