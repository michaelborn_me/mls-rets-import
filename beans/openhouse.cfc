/**
 * contains interactions with the database OpenHouse table
 */
component hint="contains interactions with the database OpenHouse table" extends="bigQuery" {
	this.table = "OpenHouse";
}