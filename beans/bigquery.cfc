<cfcomponent hint="Helps with gigantic queries for data imports">
    <cffunction name="init" hint="constructor, duh!" returnType="object">
        <cfargument name="datasource" required="true" type="string" hint="datasource so we can insert/update.">
        <cfset this.datasource = arguments.datasource />

        <!--- good little constructors return self --->
        <cfreturn this />
    </cffunction>

    <cffunction name="deleteByListingID" returntype="struct" hint="DELETE a record from one of the listing tables.">
		<cfargument name="listingID" required="true" type="string" hint="Which listing should we remove?">
        <cfargument name="tablename" type="string" required="true" hint="Select from a custom table" />

        <cfquery name="removelistingbylistingid" datasource="#this.datasource#" result="local.result">
            DELETE FROM #arguments.tablename#
            WHERE `ListingID` = <cfqueryparam value="#arguments.listingID#" cfsqltype="cf_sql_varchar">
        </cfquery>
        
        <cfreturn local.result />
    </cffunction>

    <cffunction name="getAllSafe" output="true" hint="A much safer version of getAll. Totally parameterized.">
		<cfargument name="id" required="no" type="string" default="" hint="Get records by primary key.">
		<cfargument name="fields" required="no" type="string" default="*" hint="Specify list of field names to retrieve." />
		<cfargument name="whereClause" required="false" type="string" default="1=1" hint="Literally a SQL statement to adjust the query results. Please used named parameters!" />
		<cfargument name="params" required="false" type="struct" default="#{}#" />
		<cfargument name="orderBy" required="no" type="array" default="#[]#" hint="Nested array containing column names and directions to sort by. Each row is a [column,direction] array. Like '[['submitteddate','desc'],['firstname','asc']]'">
		<cfargument name="limit" required="no" type="numeric" default="1000000" hint="What are the max number of rows to retrieve?">
		<cfargument name="offset" required="no" type="numeric" default="0" hint="For paging, return maxrows starting at position X.">
		
		<cfscript>
			local.sql = "SELECT " & arguments.fields & " FROM " & this.table;
			local.sql &= " WHERE " & arguments.whereClause;
			
			if (arguments.id > "") {
				// add id both to the parameterized query and to the parameters struct itself.
				local.sql &= " AND id=:id";
				arguments.params.id = arguments.id;
			}
			
			// ORDER BY clause
			if ( ArrayLen(arguments.orderBy) ) {
				local.sql &= " ORDER BY ";
				local.colcount = 1;
				for (local.order in arguments.orderBy) {
					local.safesortDir = local.order[2];
					
					// make the sort column safe-ish by stripping non alphanumeric characters.
					local.safeSortCol = ReReplace(local.order[1],'[^a-zA-Z0-9_-]','','ALL');
					
					// make the sort direction safe by whitelisting only two possible values.
					if ( !ListFindNoCase("asc,desc",local.safesortDir)) { local.safesortDir = "ASC"; }
					
					// Ok, so array[1] is the sort COLUMN, and array[2] is the sort DIRECTION
					local.sql &= local.safeSortCol & " " & local.safesortDir;
					if ( local.colcount < ArrayLen(arguments.orderBy) ) { local.sql &= ", "; } else { local.sql &= " "; };
					
					local.colcount++;
				}
			}
			
			// LIMIT clause gets X rows starting at row Z
			local.sql &= " LIMIT " & arguments.offset & "," & arguments.limit;
			
			// get it!
			getem = QueryExecute(local.sql, arguments.params, { datasource: this.datasource} );
		</cfscript>
		
		<cfreturn getem />
	</cffunction>

    <cffunction name="getAll" returntype="query" hint="Get X records from the current table.">
		<cfargument name="limit" required="no" default="10000" type="numeric" hint="What are the max number of rows to retrieve?">
		<cfargument name="offset" required="no" default="0" type="numeric" hint="What record should we start on?">
        <cfargument name="tablename" type="string" required="false" default="#this.table#" hint="Select from a custom table" />

        <cfquery name="local.getem" datasource="#this.datasource#">
            SELECT * FROM #arguments.tablename#
            LIMIT #arguments.offset#,#arguments.limit#
        </cfquery>
        
        <cfreturn local.getem />
    </cffunction>

    <cffunction name="getLatestError" hint="if save() function returns false, use this function to find out what happened!" returnType="struct">
        <cfreturn this.theError />
    </cffunction>
    
    <cffunction name="theQuery" output="false" returnType="void" hint="BIG query to save data.">
        <cfargument name="response" type="object" required="true" hint="Response from the RETS feed search request" />
        <cfargument name="tablename" type="string" required="false" default="#this.table#" hint="In which table should we save the data?" />
        
        <cfset local.columns = arguments.response.getColumns() />

        <cfquery name="local.insertorupdate" datasource="#this.datasource#">
            INSERT INTO #arguments.tablename# (
                <cfloop from="1" to="#arrayLen(local.columns)#" index="local.col">
                    <cfif col GT 1>,</cfif>`#local.columns[local.col]#`
                </cfloop>
            )  VALUES
                <cfloop from="0" to="#arguments.response.getRowCount()-1#" index="local.row">
                <cfset local.curRow = arguments.response.getRow(local.row) />
                
                <!--- comma separates subsequent rows --->
                <cfif row GT 0>,</cfif>
                <!--- new row in insert statement --->
                (
                <cfloop from="1" to="#arrayLen(local.columns)#" index="local.col">
                    <cfset local.null = false />
                    <cfif local.curRow[local.col] IS ""><cfset local.null = true /></cfif>
                    <cfif col GT 1>,</cfif><cfqueryparam value="#local.curRow[local.col]#" null="#local.null#">
                </cfloop>
                )
            </cfloop>
                <!--- this protects against what happens if the listing (listingID) already exists in the database. --->
                <!--- in that case, we update it, do not insert new record --->
                ON DUPLICATE KEY UPDATE
                <cfloop from="1" to="#arrayLen(local.columns)#" index="local.col">
                    <cfif col GT 1>,</cfif>#local.columns[local.col]#=VALUES(`#local.columns[local.col]#`)
                </cfloop>
        </cfquery>
    </cffunction>

    <cffunction name="save" hint="BIG query to save residentialProperties. Does some helpful error catching and stuff." returntype="boolean" output="false">
        
        <cfset local.success = true />
        <cftry>

            <cfset this.theQuery(
                argumentCollection = arguments
            ) />

            <cfcatch type="database">
                <!--- ERROR IN QUERY OR DATA! --->
                <cfset local.success = false />
                <cfset this.theError = cfcatch />
            </cfcatch>
        </cftry>

        <cfreturn local.success />
    </cffunction>
</cfcomponent>