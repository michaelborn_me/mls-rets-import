<div class="hero is-dark">
	<div class="hero-body">
		<div class="container">
			<h2 class="title">DEBUGGING</h2>
            <h3 class="subtitle">All sorts of metadata</h3>
		</div>
	</div>
</div>
<cfscript>
	// Start RETS connection
	include "inc/start.cfm";
	
	// schedule the task
	cronURL = "http://#cgi.server_name##cgi.script_name#";

	/**************
	 * BEGIN IMPORT
	 **************/
	writeOutput('<div class="section"><div class="container"><h2 class="title is-2">Pulling Debug Metadata</h2>');
	metadata = CreateObject("beans.metadata").init(
		datasource = variables.config.datasource
	);
    
    // get ready for request
    metaRequest = javaloader.create("org.realtors.rets.client.GetMetadataRequest");

    // class metadata
	include "debugging/class.cfm";

    // table metadata
	include "debugging/table.cfm";

    // object metadata
	include "debugging/object.cfm";

    // resource metadata
    include "debugging/resource.cfm";
</cfscript>