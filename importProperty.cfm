<div class="hero is-dark">
	<div class="hero-body">
		<div class="container">
			<h2 class="title">Import ResidentialProperty</h2>
		</div>
	</div>
</div>
<cfscript>
	// Start RETS connection
	include "inc/start.cfm";
	
	// schedule the task
	cronURL = "http://#cgi.server_name##cgi.script_name#";
	cfschedule(
		action		= "update",
		task		= "cny.mls.import.listings",
		url			= "#cronURL#",
		interval	= config.interval.property,
		startDate	= "5/12/2016",
		startTime 	= "2:00 AM"
	);

	/**************
	 * BEGIN IMPORT
	 **************/
	writeOutput('
		<div class="section">
			<div class="container">
				<h2 class="title is-2">Pull Listings</h2>
				<h3 class="subtitle">Search query: #variables.config.search.property#</h3>');
	
	listingObj = CreateObject("beans.property").init(
		datasource = variables.config.datasource
	);
    

	// we loop once per "class" and export everything before we move on to the next.
	local.classes = ["Commercial","LotsAndLand","MobileHome","MultiFamily","ResidentialProperty"];
	for (local.class in local.classes) {
		local.currentBatch = 0;
		do {
			// get only the records updated within the last year or month or whatever.
			// this makes queries for large sets of data just way faster.
			searchQuery = variables.config.search.property;
			local.searchRequest = javaloader.create("org.realtors.rets.client.SearchRequest").init("Property",local.class,searchQuery);

			// get X records starting at Y where Y = (x*batchsize)
			local.searchRequest.setLimit(config.batchSize.residential);
			local.searchRequest.setOffset(local.currentBatch * config.batchSize.residential);
			local.searchResponse = retsSession.search(local.searchRequest);
			local.rowCount = local.searchResponse.getRowCount();

			writeOutput('<h3 class="title is-5">Pulling #local.class# properties</h3><p>#local.rowCount# result(s) in batch ###local.currentBatch#</p>');
			
			/*
			if (config.debug) {
				// DUMP COLUMNS FOR DEBUGGING
				// get the current row
				if ( local.rowCount > 0 ) {
					local.curRow = local.searchResponse.getRow(0);

					// get all columns
					local.columns = local.searchResponse.getColumns();
					
					for (local.col = 1; local.col < ArrayLen(local.columns); local.col++) {
						writeOutput("<pre>ALTER TABLE PROPERTY ADD COLUMN " & local.columns[local.col] & " VARCHAR(100);</pre>");
					}
				}
			}
			*/
			if (local.searchResponse.getRowCount() == 0) {
				writeOutput('<p class="notification is-warning">Found zero rows.</p>');
			} else {
				// run the query, given all the data we have
				sqlSuccess = listingObj.save(
					response = local.searchResponse,
					tablename = local.class
				);
				if (! sqlSuccess ) {
					// get error info
					writeOutput('<div class="notification is-danger"><p>ERROR: ' & listingObj.getLatestError().detail & '</p>');
					writeDump(listingObj.getLatestError(),false);
					writeOutput('</div>');
				} else {
					writeOutput('<p class="notification is-success">Saved results to database</p>');
				}
			}

			// increment the current batch number so we can get the NEXT batch
			local.currentBatch++;
			// as long as we were able to retrieve the max, then keep looping!
		} while(local.searchResponse.getRowCount() == config.batchSize.residential);
	}

	// success
	writeoutput('<p class="notification is-success">Done! Saved ' & local.currentBatch & " batches of " & config.batchSize.residential & " each.</p>");
	writeOutput('</div></div>');
</cfscript>