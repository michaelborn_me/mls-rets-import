<div class="hero is-dark">
	<div class="hero-body">
		<div class="container">
			<h2 class="title">Import Media Objects</h2>
		</div>
	</div>
</div>
<cfscript>
	// Start RETS connection
	include "inc/start.cfm";
	
	// schedule the task
	cronURL = "http://#cgi.server_name##cgi.script_name#";
	cfschedule(
		action		= "update",
		task		= "cny.mls.import.objects",
		url			= "#cronURL#",
		interval	= config.interval.object,
		startDate	= "5/12/2016",
		startTime 	= "2:30 AM"
	);

	/**************
	 * BEGIN IMPORT
	 **************/
	writeOutput('
		<div class="section">
			<div class="container">
				<h2 class="title is-2">Pull Listing Media Objects</h2>');
	
    setting requesttimeout=300;

	// @cite https://github.com/trulia/trulia-java-rets-client
	variables.listingObj = CreateObject("beans.property").init(
		datasource = variables.config.datasource
	);
	variables.ObjectReq = javaloader.create("org.realtors.rets.client.GetObjectRequest");
	
	cftimer( label="Total Import Time", type="outline" ) {

		// we loop once per "class" and export everything before we move on to the next.
		local.classes = ["Commercial","LotsAndLand","MobileHome","MultiFamily","ResidentialProperty"];
		for (local.class in local.classes) {
			writeOutput('<div class="box"><h3 class="title is-3">Pulling #local.class# properties</h3>');

			// get ready to batch-process images
			variables.currentBatch = 0;
			do {
				// we grab the records in groups of 100 (see variables.config.batchSize.binaries)
				// and store them in the db.
				variables.listings = variables.listingObj.getAll(
					limit = variables.config.batchSize.binaries,
					offset = variables.currentBatch * variables.config.batchSize.binaries,
					tablename = local.class
				);
				
				// we loop over every listing in the current recordset
				// note that we "page" the listings to avoid problems.
				cfloop(query = variables.listings) {
					writeOutput('<h3 class="title is-5">Downloading media objects for listing: ' & listings.listingID);
					
					// loop through all listings
					// and retrieve all image/video/etc. objects from each one.
					variables.searchResource = "Property";
					variables.searchType = "LargePhoto";
					variables.req = variables.objectReq.init(variables.searchResource,variables.searchType);

					// ask RETS to return all objects for the listing.
					variables.req.addObject(listings.listingID,"*");
					variables.objects = retsSession.getObject(variables.req).iterator();

					if (variables.objects.hasNext()) {
						// if found any images, then create a folder for the listing to contain those images.
						local.imageDir = ExpandPath(config.folders.images) & "listing_" & variables.listings.listingID;
						if (!directoryExists(local.imageDir)) {
							directoryCreate(local.imageDir);
						}

						// DISABLED: create folders for videos and documents
						// local.videoDir = ExpandPath(config.folders.videos) & "listing_" & variables.listings.listingID;
						// local.docDir = ExpandPath(config.folders.documents) & "listing_" & variables.listings.listingID;
						// if (!directoryExists(local.videoDir)) {
						// 	directoryCreate(local.videoDir);
						// }
						// if (!directoryExists(local.docDir)) {
						// 	directoryCreate(local.docDir);
						// }
					}

					// loop once per image per listing, so a listing with 10 images has 10 objects to save to disk.
					cftimer( label="Time to download images", type="outline" ) {

						while (variables.objects.hasNext()) {

							local.fileObject = variables.objects.next();
							if ( local.fileObject.getType() != "image/jpeg") {
									// throw warning and skip loop
									if ( config.debug ) {
										writeDump(local.fileObject);
										writeDump(local.fileObject.getObjectID());
										writeDump(local.fileObject.getType());
										writeDump(local.fileObject.getLocation());
										writeDump(local.fileObject.getDescription());
										writeOutput(variables.config.rets_uri & "/getobject?Resource=Property&ID=" & variables.listings.listingID & ":1&Type=Photo");
									}
									continue;
							} else {
								// set/get filename
								local.filename = "img_#variables.listings.listingID#_#local.fileObject.getObjectID()#.jpg";
								local.filename = "#local.imageDir#/#local.filename#";

								// read the buffered image
								local.ImageIO = CreateObject("java","javax.imageio.ImageIO");
								local.img = local.ImageIO.read(local.fileObject.getInputStream());
								// write it to disk
								local.ImageIO.write( local.img, "jpg", CreateObject("java","java.io.File").init(local.filename) );
							}
						}// end loop through objects

						// Ok, we're done grabbing the images for this listing.
						// grab the next batch of images for the next listing.
					}
				}

				// increment to get the next batch.
				variables.currentBatch++;

				// as long as we were able to retrieve the max, then keep looping!
			} while(listings.recordCount == variables.config.batchSize.binaries);
			
			writeOutput('</div>');
		} // end loop through classes which enables us to retrieve images for mobile homes separate from images for residential.

	} // end timer

	// debugging
	writeoutput('<p class="notification is-success">Done! Saved ' & variables.currentBatch & " batches of " & variables.config.batchSize.binaries & " each.</p>");
	writeOutput('</div></div>');
</cfscript>