<div class="hero is-dark">
	<div class="hero-body">
		<div class="container">
			<h2 class="title">Import Open House Data</h2>
		</div>
	</div>
</div>
<cfscript>
	// Start RETS connection
	include "inc/start.cfm";
	
	// schedule the task
	cronURL = "http://#cgi.server_name##cgi.script_name#";
	cfschedule(
		action		= "update",
		task		= "cny.mls.import.openhouse",
		url			= "#cronURL#",
		interval	= config.interval.openhouse,
		startDate	= "5/12/2016",
		startTime 	= "2:10 AM"
	);
	
	/**************
	 * BEGIN IMPORT
	 **************/
	writeOutput('
		<div class="section">
			<div class="container">
				<h2 class="title is-2">Pull Open House data</h2>
				<h3 class="subtitle">Search query: #variables.config.search.openhouse#</h3>');
	
	openhouseObj = CreateObject("beans.openhouse").init(
		datasource = variables.config.datasource
	);

    currentBatch = 0;
	do {
		if (currentBatch > 0) {
			structClear(local.searchResponse);
		}
		// we grab the records in groups of 100
		// and store them in the db.
		
		// NOW DO A SEARCH
		searchFor = "OpenHouse";
		searchClass = "OpenHouse";
		// get only the records updated within the last year or month or whatever.
		// this makes queries for large sets of data just way faster.
		searchQuery = variables.config.search.openhouse;
		searchRequest = javaloader.create("org.realtors.rets.client.SearchRequest").init(searchFor,searchClass,searchQuery);

		// get X records starting at Y where Y = (x*batchsize)
		searchRequest.setLimit(config.batchSize.openhouse);
		searchRequest.setOffset(currentBatch * config.batchSize.openhouse);
		local.searchResponse = retsSession.search(searchRequest);
		local.rowCount = local.searchResponse.getRowCount();

		writeOutput('<h3 class="title is-5">Search Results: batch ##' & currentBatch & " found " & local.rowCount & " result(s)</h3>");
		
		// run the query, given all the data we have
		sqlSuccess = openhouseObj.save(
			response = local.searchResponse
		);
		
		if (! sqlSuccess ) {
			// get error info
			writeOutput('<div class="notification is-danger"><p>ERROR: ' & openhouseObj.getLatestError().detail & '</p>');
			writeDump(openhouseObj.getLatestError(),false);
			writeOutput('</div>');
		} else {
			writeOutput('<p class="notification is-success">Saved results to database</p>');
		}

		// increment the current batch number so we can get the NEXT batch
		currentBatch++;
		// as long as we were able to retrieve the max, then keep looping!
	} while(local.searchResponse.getRowCount() == config.batchSize.openhouse);

	// debugging
	writeoutput('<p class="notification is-success">Done! Saved ' & currentBatch & " batches of " & config.batchSize.openhouse & " each.</p>");
	writeOutput('</div></div>');
</cfscript>